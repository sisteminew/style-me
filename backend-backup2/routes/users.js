const express = require("express")
const users = express.Router();
const DB = require('../db/dbConn.js')

const Cookies = require('cookies')


users.post('/login', async (req, res, next) => {
    try {
        console.log(req.body);
        const username = req.body.username;
        const password = req.body.password;
        if (username && password) {
            const queryResult = await DB.AuthUser(username, password)
            if (queryResult.length > 0) {
                if (password === queryResult[0].password) {
                    req.session.user = queryResult
                    req.session.authenticated = true
                    const cookies = new Cookies(req,res)
                    const cookieVal = encodeURIComponent(JSON.stringify(queryResult[0]))
                    cookies.set('user', cookieVal, { httpOnly: true, maxAge: 1000 * 60 * 24 * 10 })
                    res.status(200).json({ user: queryResult[0], status: { success: true, msg: "Logged in!" } })
                } 
                else {
                    res.status(200).json({ user: null, status: { success: false, msg: "Password incorrect" } })
                    console.log("incorrect password")
                }
            } else {
                res.status(200).send({ user: null, status: { success: false, msg: "Username not registered" } })
            }
        }
        else {
            res.status(200).send({ authenticated: false, user: null, status: { success: false, msg: "Input element missing" } })
            console.log("Please enter Username and Password!")
        }
        res.end();
    } catch (err) {
        console.log(err)
        res.sendStatus(500)
        next()
    }
});

users.get('/session', async (req, res, next) => {
    try {
        if (req.session) {
            const user = await DB.getUserById(user_id)
            res.json({
                authenticated: true,
                user: user[0]
            })
        } 
        else {
            res.json({ authenticated: false})
        }


    } catch (error) {
        res.sendStatus(500)
    }
})

//Inserts a new user in our database id field are complete
users.post('/register', async (req, res, next) => {
    try {
        const username = req.body.username
        const password = req.body.password
        const email = req.body.email
        if (username && password && email) {
            const queryResult = await DB.AddUser(username, email, password);
            if (queryResult.affectedRows) {
                const cookies = new Cookies (req,res) 
                //const token = cookies.get('auth_token')
                cookies.set('user', JSON.stringify({ username }), { httpOnly: true, maxAge: 1000 * 60 * 60 * 24 * 10})
                res.status(200).send({ status: { success: true, msg: "New user created" } })
                console.log("New user added!!")
            }
        }
        else {
            res.status(200).send({ status: { success: false, msg: "Input element missing" } })
            console.log("A field is missing!")
        }
        res.end();
    } catch (err) {
        console.log(err)
        res.status(500).send({ status: { success: false, msg: err } })
        next(err)
    }

});

users.post('/logout', async (req, res, next) => {
    try{
    const cookies = new Cookies (req, res);
    cookies.set('user', '', { expires: new Date(0), httpOnly: true })
    res.status(200).send({ status: { success: true, msg: "Logout successful!"}})}

    catch(err) {
        console.error('Logout error: ', err)
        res.sendStatus(500).send({ ststus: { success: false, msg: 'Logout failed'}})
    }
})

users.get('/preferences', async (req, res, next) => {
        
        if (!req.session.userId) return res.redirect('/login');
        res.sendFile('') //not sure
})

users.post('/preferences', async (req, res, next) => {
    try {

        if (!req.session.user_id) {
            return res.status(401).json({ success: false, msg: 'Please log in or sign up.' })
        }

        const user_id = req.session.user_id
        const option_selected = req.body.option_selected

        const style_id = determineStyle(option_selected)

        await DB.AddPreferences(user_id, option_selected)
        await DB.UserStyle(user_id, style_id)

        res.status(200).json({ success: true, style: style_id });
    } catch (err) {
        console.log(err)
        res.sendStatus(500).json({ success: false, msg: 'There has been an error.'})
        next()
}})


function determineStyle(questions) {
    const styles = {
        business: ['business'],
        rocker: ['rocker']
    }

    let businessScore, rockerScore = 0

    questions.forEach(question => {
        if (business.includes(question)) businessScore++
        if (rocker.includes(question)) rockerScore++
    })

    if (businessScore > 0) return 'business'
    if (rockerScore > 0) return 'rocker'
}



module.exports = users
