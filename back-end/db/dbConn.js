const mysql = require('mysql2');

const conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS, 
    database: process.env.DB_DATABASE
  })

  setInterval(() => {
    conn.query('SELECT 1', (error, result) => {
        if (error) {
            console.error('Error with keep-alive query:', error);
        } else {
            console.log('Keep-alive query executed successfully');
        }
    });
}, 60000);


 conn.connect((err) => {
      if(err){
          console.log("ERROR: " + err.message);
          return;    
      }
      console.log('Connection established');
    })


    let dataPool={}
  
dataPool.saveImageMetadata = (fileName, filePath, title) => {
    return new Promise((resolve, reject) => {
        conn.query('INSERT INTO images (filename, filepath, title) VALUES (?, ?, ?)', [fileName, filePath, title],
            (err, res) => { if (err) {
                    return reject(err);
                }
                return resolve(res);
            }
        );
    });
};




// Example searchImagesByTitle function
dataPool.searchImagesByTitle = (title) => {
  return new Promise((resolve, reject) => {
    conn.query('SELECT filename, filepath, title FROM images WHERE title LIKE ?', [`%${title}%`], 
      (err, results) => {
        if (err) {
          return reject(err);
        }
        return resolve(results);
      }
    );
  });
};


dataPool.getAllImages = () => {
  return new Promise((resolve, reject) => {
      conn.query('SELECT filename, filepath, title FROM images', [],
          (err, results) => {
              if (err) {
                  return reject(err);
              }
              return resolve(results);
          }
      );
  });
};

dataPool.addComment = (filename, comment_text) => {
  return new Promise((resolve, reject) => {
    const query = 'INSERT INTO comments (filename, comment_text) VALUES (?, ?)';
    conn.query(query, [filename, comment_text], (err, results) => {
      if (err) {
        return reject(err);
      }
      return resolve(results);
    });
  });
}

dataPool.getCommentsByFilename = (filename) => {
  return new Promise((resolve, reject) => {
    const query = 'SELECT comment_text, created_at FROM comments WHERE filename = ?';
    conn.query(query, [filename], (err, results) => {
      if (err) {
        return reject(err);
      }
      return resolve(results);
    });
  });
}


dataPool.allNovice=()=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`SELECT * FROM novice`, (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.oneNovica=(novice_id)=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`SELECT * FROM novice WHERE novice_id = ?`, novice_id, (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.creteNovica=(title,slug,text,file)=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`INSERT INTO novice (title,slug,text, file) VALUES (?,?,?,?)`, [title, slug, text, file], (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.AuthUser=(username)=>
{
  return new Promise ((resolve, reject)=>{
    conn.query('SELECT * FROM user WHERE username=?', username, (err,res, fields)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })  
	
}

dataPool.AddUser=(username,email,password)=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`INSERT INTO user (username,password,email) VALUES (?,?,?)`, [username,password,email ], (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}
dataPool.addOrUpdateVote = (color) => {
  return new Promise((resolve, reject) => {
      // Check if the color already exists
      const checkQuery = 'SELECT count FROM votes WHERE color = ?';
      conn.query(checkQuery, [color], (err, results) => {
          if (err) return reject(err);

          if (results.length > 0) {
              // Color exists, update the count
              const updateQuery = 'UPDATE votes SET count = count + 1 WHERE color = ?';
              conn.query(updateQuery, [color], (err, res) => {
                  if (err) return reject(err);
                  return resolve(res);
              });
          } else {
              // Color does not exist, insert a new row
              const insertQuery = 'INSERT INTO votes (color, count) VALUES (?, 1)';
              conn.query(insertQuery, [color], (err, res) => {
                  if (err) return reject(err);
                  return resolve(res);
              });
          }
      });
  });
};


dataPool.getVotes = () => {
  return new Promise((resolve, reject) => {
      const query = 'SELECT * FROM votes';
      conn.query(query, (err, results) => {
          if (err) return reject(err);
          // Convert the results to a more accessible format
          const votes = results.reduce((acc, row) => {
              acc[row.color] = row.count;
              return acc;
          }, {});
          return resolve(votes);
      });
  });
};



dataPool.AddPreferences=(option_selected) => {
  return new Promise ((resolve, reject) =>{
    const query = 'INSERT INTO preferences (option_selected) VALUES (?)'
      conn.query(query, [option_selected], (err,res) =>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}



dataPool.GetPreferences = () => {
  return new Promise((resolve, reject) => {
    const query = 'SELECT option_selected, COUNT(*) AS count FROM preferences GROUP BY option_selected';
    conn.query(query, (err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};

dataPool.UserStyle = (preferences_id) => { //ovdje u style_id treba stavit rocker il business ovisi koji je look u pitanju 
  return new Promise ((resolve, reject) => {
    conn.query('UPDATE style SET style_id = ?')
  })
}



dataPool.createNovica = (title, text) => {
  return new Promise((resolve, reject) => {
    conn.query(`INSERT INTO novice (title, text) VALUES (?, ?)`, [title, text], (err, res) => {
      if (err) { return reject(err); }
      return resolve(res);
    });
  });
};

dataPool.incrementLikes = (novice_id) => {
  return new Promise((resolve, reject) => {
    conn.query(`UPDATE novice SET likes = likes + 1 WHERE novice_id = ?`, [novice_id], (err, res) => {
      if (err) { return reject(err); }
      return resolve(res);
    });
  });
};


module.exports = dataPool;

