const express = require("express");
const novice = express.Router();
const DB = require('../db/dbConn.js'); // Ensure you have DB functions for your queries

// Gets all the news in the DB
novice.get('/', async (req, res, next) => {
  try {
    const queryResult = await DB.allNovice();
    res.json(queryResult);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
    next();
  }
});

// Gets one news based on the novice_id
novice.get('/:novice_id', async (req, res, next) => {
  try {
    const queryResult = await DB.oneNovica(req.params.novice_id);
    res.json(queryResult);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
    next();
  }
});

// Inserts one news to the database
novice.post('/', async (req, res, next) => {
  const { title, text } = req.body;

  if (!title || !text) {
    res.status(400).send({ success: false, msg: "Missing input fields" });
    return;
  }

  try {
    const queryResult = await DB.createNovica(title, text);
    if (queryResult.affectedRows) {
      res.status(200).send({ success: true, msg: "News item added" });
    }
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
    next();
  }
});

// Endpoint to like a news item
novice.post('/like/:novice_id', async (req, res, next) => {
  try {
    const queryResult = await DB.incrementLikes(req.params.novice_id);
    if (queryResult.affectedRows) {
      res.status(200).send({ success: true });
    } else {
      res.status(400).send({ success: false });
    }
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
    next();
  }
});

module.exports = novice;
