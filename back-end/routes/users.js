const express = require("express")
const users = express.Router();
const DB = require('../db/dbConn.js')

const Cookies = require('cookies')

const path = require('path');

  

users.post('/login', async (req, res, next) => {
    try {
        console.log(req.body);
        const { username, password } = req.body
        if (username && password) {
            const queryResult = await DB.AuthUser(username, password)
            if (queryResult.length > 0) {
                if (password === queryResult[0].password) {
                    req.session.user = queryResult[0]
                    req.session.authenticated = true
                    const cookies = new Cookies(req,res)
                    const cookieVal = encodeURIComponent(JSON.stringify(queryResult[0]))
                    cookies.set('user', cookieVal, { httpOnly: true, maxAge: 1000 * 60 * 24 * 10 })
                    res.status(200).json({ user: queryResult[0], status: { success: true, msg: "Logged in!" } })
                } 
                else {
                    res.status(200).json({ user: null, status: { success: false, msg: "Password incorrect" } })
                    console.log("incorrect password")
                }
            } else {
                res.status(200).send({ user: null, status: { success: false, msg: "Username not registered" } })
            }
        }
        else {
            res.status(200).send({ logged: false, user: null, status: { success: false, msg: "Input element missing" } })
            console.log("Please enter Username and Password!")
        }
        res.end();
    } catch (err) {
        console.log(err)
        res.sendStatus(500)
        next()
    }
});

users.get('/session', async (req, res, next) => {
    try {
        if (req.session.authenticated) {
            const user_id = req.session.user.id

            const user = await DB.getUserById(user_id)
            res.json({ authenticated: true, user: user[0] })
        } 
        else {
            res.json({ authenticated: false})
        }


    } catch (error) {
        res.sendStatus(500)
    }
})

//Inserts a new user in our database id field are complete
users.post('/register', async (req, res, next) => {
    try {
        const username = req.body.username
        const password = req.body.password
        const email = req.body.email
        if (username && password && email) {
            const queryResult = await DB.AddUser(username, email, password);
            if (queryResult.affectedRows) {
                const cookies = new Cookies (req,res) 
                //const token = cookies.get('auth_token')
                cookies.set('user', JSON.stringify({ username }), { httpOnly: true, maxAge: 1000 * 60 * 60 * 24 * 10})
                res.status(200).send({ status: { success: true, msg: "New user created" } })
                console.log("New user added!!")
            }
        }
        else {
            res.status(200).send({ status: { success: false, msg: "Input element missing" } })
            console.log("A field is missing!")
        }
        res.end();
    } catch (err) {
        console.log(err)
        res.status(500).send({ status: { success: false, msg: err } })
        next(err)
    }

});

users.get('/logout', async (req, res, next) => {
    try{

        req.session.destroy( err => {
            if (err) {
                return res.status(500).send({ status: { success: false, msg: "Still signed in"}})
            }
        }
        )
        console.log("Logged out")
        res.json({ success: true, msg: "Logged out"})
    }
    catch(err) {
        console.error('Logout error: ', err)
        res.sendStatus(500).send({ status: { success: false, msg: 'Logout failed'}})
    }
})


const votes = {
    pink: 0,
    blue: 0
}

users.get('/votes', async (req, res) => {
    try {
        const votes = await DB.getVotes(); // Use the dataPool to get votes
        res.json(votes);
    } catch (error) {
        console.error('Failed to fetch votes:', error);
        res.status(500).json({ error: 'Failed to fetch votes' });
    }
});

users.post('/vote', async (req, res) => {
    const { color } = req.body;
    try {
        await DB.addOrUpdateVote(color); // Use dataPool to add or update vote
        res.status(200).json({ success: true });
    } catch (error) {
        console.error('Failed to update vote:', error);
        res.status(500).json({ error: 'Failed to update vote' });
    }
});





users.post('/preferences', async (req, res) => {
     const { option_selected } = req.body;
   
     try {
       await DB.AddPreferences( option_selected);
       res.json({ success: true, msg: "Preferences saved successfully" });
     } catch (err) {
       console.error("Error saving preferences:", err);
       res.status(500).json({ success: false, msg: "Error saving preferences" });
     }
   });

users.get('/count', async (req, res) => {
    try {
      const preferenceCounts = await DB.GetPreferences();
      console.log("Preference Counts from DB:", preferenceCounts); // Log the result

      const counts = preferenceCounts.reduce((acc, row) => {
        acc[row.option_selected] = row.count;
        return acc;
      }, {});
  
      res.json({
        businessCount: counts['business'] || 0,
        rockerCount: counts['rocker'] || 0
      });
    } catch (err) {
      console.error('Error fetching preference counts:', err);
      res.status(500).json({ error: 'Failed to fetch counts' });
    }
  });

function determineStyle(questions) {
    const styles = {
        business: ['business'],
        rocker: ['rocker']
    }

    let businessScore = 0, rockerScore = 0

    questions.forEach(question => {
        if (styles.business.includes(question)) businessScore++
        if (styles.rocker.includes(question)) rockerScore++
    })

    if (businessScore > 0) return 'business'
    if (rockerScore > 0) return 'rocker'
}



module.exports = users
