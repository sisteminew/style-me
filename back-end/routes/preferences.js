const express = require('express');
const preferences = express.Router();
const DB = require('../db/dbConn'); 

// Route to handle preferences submission
preferences.post('/preferences', async (req, res) => {
 // const user_id = req.session.user[0].id; // Assuming user ID is stored in session
  const { option_selected } = req.body;

 // if (!user_id || !option_selected) {
 //   return res.status(400).json({ success: false, msg: "User ID or preferences not provided" });
 // }

  try {
    // Insert preferences into the database
    await DB.AddPreferences( option_selected);
    res.json({ success: true, msg: "Preferences saved successfully" });
  } catch (err) {
    console.error("Error saving preferences:", err);
    res.status(500).json({ success: false, msg: "Error saving preferences" });
  }
});

module.exports = preferences;
