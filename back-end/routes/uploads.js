const express = require('express');
const multer = require('multer');
const path = require('path');
const DB = require('../db/dbConn'); // Adjust the path to your database connection file
const fs = require('fs');

const uploads = express.Router();

// Multer configuration
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'imageuploads/');
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

// Route to handle file upload
uploads.post('/', upload.single('file'), async (req, res) => {
  if (!req.file) {
    return res.status(400).json({ success: false, msg: 'No file uploaded' });
  }

  const { filename } = req.file;
  const filePath = `imageuploads/${filename}`;
  const title = req.body.title || 'Untitled'; 

  try {
    await DB.saveImageMetadata(filename, filePath, title); // Adjust method as needed
    res.status(200).json({ success: true, filePath: filePath });
  } catch (error) {
    console.error('Error saving image metadata:', error);
    res.status(500).json({ success: false, msg: 'Error saving image metadata' });
  }
});

// Route to list all uploaded images
uploads.get('/', async (req, res) => {
  fs.readdir('imageuploads/', async (err, files) => {
    if (err) {
      return res.status(500).json({ success: false, msg: 'Failed to retrieve images' });
    }

    // Fetch metadata for all files
    try {
      const images = await DB.getAllImages(); // Adjust method as needed
      res.status(200).json(images.map(image => ({
        filename: image.filename,
        title: image.title
      })));
    } catch (error) {
      console.error('Error retrieving image metadata:', error);
      res.status(500).json({ success: false, msg: 'Error retrieving image metadata' });
    }
  });
});

// Route to search images by title
uploads.get('/search', async (req, res) => {
  const title = req.query.title;
  try {
    const images = await DB.searchImagesByTitle(title); // Adjust method as needed
    res.status(200).json(images.map(image => ({
      filename: image.filename,
      title: image.title
    })));
  } catch (error) {
    console.error('Error searching images:', error);
    res.status(500).json({ success: false, msg: 'Error searching images' });
  }
});

// Route to add a comment
uploads.post('/comments', async (req, res) => {
  const { filename, comment_text } = req.body;
  try {
    await DB.addComment(filename, comment_text);
    res.status(200).json({ success: true });
  } catch (error) {
    console.error('Error adding comment:', error);
    res.status(500).json({ success: false, msg: 'Error adding comment' });
  }
});

// Route to get comments by filename
uploads.get('/comments/:filename', async (req, res) => {
  const filename = req.params.filename;
  try {
    const comments = await DB.getCommentsByFilename(filename);
    res.status(200).json(comments);
  } catch (error) {
    console.error('Error retrieving comments:', error);
    res.status(500).json({ success: false, msg: 'Error retrieving comments' });
  }
});

module.exports = uploads;
