
import { Component } from "react";
import React from 'react';
import { ABOUT, NOVICE, ADDNEW, SIGNUP, LOGIN, NOVICA, HOME, LOGOUT, UPLOAD, PREFERENCES, COLOUR } from "./Utils/Constants"
import HomeView from "./CustomComponents/HomeView";
import AboutView from "./CustomComponents/AboutView";
import NoviceView from "./CustomComponents/NoviceView";
//import AddNovicaView from "./CustomComponents/AddNovicaView";
import SignupView from "./CustomComponents/SignupView";
import LoginView from "./CustomComponents/LoginView";
import SingleNovicaView from "./CustomComponents/SingleNovicaView";
import FilesUploadComponent from "./CustomComponents/FilesUpload";
import Colour from "./CustomComponents/Colour";

import axios from "axios";
import { API_URL } from "./Utils/Configuration";
import Cookies from 'universal-cookie';
import PreferencesForm from "./CustomComponents/PreferencesForm";

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import './App.css';

const cookies = new Cookies();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CurrentPage: HOME,
      Novica: 1,
      status: {
        success: null,
        msg: ""
      },
      user: null,
      isDarkMode: JSON.parse(localStorage.getItem('darkMode')) || false
    };
  }

  QGetView(state) {
    const page = state.CurrentPage;
    switch (page) {
      case ABOUT:
        return <AboutView />;
      case NOVICE:
        return <NoviceView QIDFromChild={this.QSetView} />;
      //case ADDNEW:
        //return <AddNovicaView />;
      case SIGNUP:
        return <SignupView />;
      case COLOUR:
          return <Colour />;
      case LOGIN:
        return <LoginView QUserFromChild={this.QSetLoggedIn} />;
      case LOGOUT:
        return <HomeView />;
      case UPLOAD:
        return <FilesUploadComponent />;
      case NOVICA:
        return <SingleNovicaView data={state.Novica} QIDFromChild={this.QSetView} />;
      case PREFERENCES:
        return <PreferencesForm/>;
      default:
        return <HomeView />;
    }
  };

  QSetView = (obj) => {
    this.setState(this.state.status = { success: null, msg: "" })

    console.log("QSetView");
    this.setState({
      CurrentPage: obj.page,
      Novica: obj.id || 0
    });
  };


  QPostLogout = () => {
    const request = axios.create ({
      timeout: 50000,
      withCredentials: true
    })

    request.get(API_URL + 'users/logout', {}, { withCredentials: true })
    
    .then(response => {
      if (response.status === 200) {
        console.log(response.data)
        this.setState({ status: response.data, user: null })
      }
      else {
        console.error("Unexpected error.")
      }
    })

    .catch(error => {
      console.error("Unexpected error!")
    })
  }


  toggleDarkMode = () => {
    this.setState(prevState => {
      const newMode = !prevState.isDarkMode;
      localStorage.setItem('darkMode', JSON.stringify(newMode));
      return { isDarkMode: newMode };
    });
  };

  render() {
    const { isDarkMode } = this.state;
    return (
      <div id="APP" className={`box ${isDarkMode ? 'dark-mode' : 'light-mode'}`}>
        <div id="menu" className="column">
          <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="container-fluid">
              <a
                //onClick={() => this.QSetView({ page: "home" })}
                onClick={this.QSetView.bind(this, { page: "home" })}
                className="navbar-brand"
                href="#"
              >
                Home
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>

              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: ABOUT })}
                      className="nav-link "
                      href="#"
                    >
                      About
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: NOVICE })}
                      onClick={this.QSetView.bind(this, { page: NOVICE })}
                      className="nav-link "
                      href="#"
                    >
                      News
                    </a>
                  </li>


                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: ADDNEW })}
                      onClick={this.QSetView.bind(this, { page: COLOUR })}
                      className="nav-link"
                      href="#"
                    >
                      Best outfit competition
                    </a>
                  </li>


                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: ADDNEW })}
                      onClick={this.QSetView.bind(this, { page: UPLOAD })}
                      className="nav-link"
                      href="#"
                    >
                      Outfit catalogue
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: SIGNUP })}
                      onClick={this.QSetView.bind(this, { page: SIGNUP })}
                      className="nav-link "
                      href="#"
                    >
                      Sign up
                    </a>
                  </li>

                 <li className="nav-item" ><a onClick={this.QSetView.bind(this, { page: LOGIN })}
                      className="nav-link " href="#"> Login </a>
                 </li>
                
                 <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: PREFERENCES })}
                      className="nav-link "
                      href="#"
                    >
                      Preferences
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      onClick={() => { this.QSetView({ page: LOGOUT }); this.QPostLogout(this)}}
                      className="nav-link "
                      href="#"
                    >
                      LOGOUT BUTTON
                    </a>
                  </li>

                  <li className="nav-item">
                  

                  <button onClick={this.toggleDarkMode} className="btn btn-secondary">
                    
                    Toggle {this.state.isDarkMode ? 'Light' : 'Dark'} Mode
                  </button>
                

                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
        <div id="viewer" className="row container">
          {this.QGetView(this.state)}
        </div>
      </div>
    );
  }
}




export default App;
