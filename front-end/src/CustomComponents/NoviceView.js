import React from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class NewsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      novica: {
        title: "",
        text: ""
      },
      status: {
        success: null,
        msg: ""
      },
      Novice: [],
      showAddNews: true
    };
  }

  componentDidMount() {
    this.loadNews();
  }

  loadNews = () => {
    axios.get(API_URL + '/novice')
      .then(response => {
        this.setState({ Novice: response.data });
      })
      .catch(error => {
        console.error("Error fetching news:", error);
      });
  };

  QGetTextFromField = (e) => {
    const { name, value } = e.target;
    this.setState(prevState => ({
      novica: {
        ...prevState.novica,
        [name]: value
      }
    }));
  };

  QPostNovica = async () => {
    const { title, text } = this.state.novica;

    if (!title || !text) {
      this.setState({
        status: { success: false, msg: "Missing input fields" }
      });
      return;
    }

    try {
      const response = await axios.post(API_URL + '/novice', {
        title,
        text
      });
      this.setState({
        status: response.data
      });
      this.loadNews();
    } catch (error) {
      this.setState({
        status: { success: false, msg: "An error occurred while adding the news" }
      });
    }
  };

  toggleView = () => {
    this.setState(prevState => ({
      showAddNews: !prevState.showAddNews
    }));
  };

  handleLike = (novice_id) => {
    axios.post(API_URL + '/novice/like/' + novice_id)
      .then(response => {
        if (response.data.success) {
          this.loadNews();
        }
      })
      .catch(error => {
        console.error("Error liking the news item:", error);
      });
  };

  render() {
    const { showAddNews, novica, status, Novice } = this.state;

    return (
      <div className="container" style={{ maxWidth: '1000px', margin: 'auto' }}>
        <button
          className="btn btn-secondary small-btn"
          onClick={this.toggleView}
        >
          {showAddNews ? "View News" : "Add News"}
        </button>

        {showAddNews ? (
          <div className="card" style={{ margin: "10px", padding: "10px" }}>
            <h3>Add News</h3>
            <div className="mb-3">
              <label className="form-label">Title</label>
              <input
                name="title"
                type="text"
                value={novica.title}
                onChange={this.QGetTextFromField}
                className="form-control"
                placeholder="Title..."
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Text</label>
              <textarea
                name="text"
                value={novica.text}
                onChange={this.QGetTextFromField}
                className="form-control"
                rows="3"
              ></textarea>
            </div>
            <button
              className="btn btn-primary small-btn"
              onClick={this.QPostNovica}
            >
              Send
            </button>

            {status.success ? (
              <p className="alert alert-success" role="alert">
                {status.msg}
              </p>
            ) : (
              status.msg && (
                <p className="alert alert-danger" role="alert">
                  {status.msg}
                </p>
              )
            )}
          </div>
        ) : (
          <div className="row row-cols-1 row-cols-md-3 g-4">
            {Novice.length > 0 ? (
              Novice.map(d => (
                <div className="col" key={d.novice_id}>
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">{d.title}</h5>
                      <p className="card-text">{d.text}</p>
                      <p className="card-text">Likes: {d.likes}</p>
                    </div>
                    <button
                      onClick={() => this.handleLike(d.novice_id)}
                      className="btn btn-primary small-btn"
                    >
                      Like
                    </button>
                    <button
                      onClick={() => this.props.QIDFromChild({ page: "novica", id: d.novice_id })}
                      className="btn btn-secondary small-btn"
                    >
                      Read more
                    </button>
                  </div>
                </div>
              ))
            ) : (
              "Loading..."
            )}
          </div>
        )}
      </div>
    );
  }
}

export default NewsPage;
