
import React from "react";
//import { API_URL } from "../Utils/Configuration";



import { API_URL } from "../Utils/Configuration";

import fem_black_rock_day from '../outfits/fem_black_rock_day.jpg';
import fem_col_bus_day from '../outfits/fem_col_bus_day.jpg';
import male_col_bus_night from '../outfits/male_col_bus_night.jpg';
import male_black_rock_day from '../outfits/male_black_rock_day.jpg';


class HomeView extends React.Component{
    render(){
        return(
          <div className="box" style={{margin:"175px"}}>
            <div className="box-body">
                <h5 className="box-title">Welcome to the Style Me helper!</h5>
                <p className="box-text">In this tab you can find inspiration from various outfits. </p>
                <p className="box-text">Take a look at the different tabs in the ribbon above to see what else our app offers! </p>
            </div>

            <div className="image-container" style={{margin:"10px"}}>
            <img src={male_black_rock_day} alt="Male black rocker daytime" style={{ width: '250px', height: '400px', margin: '10px', border: '2px solic #ccc' }} />
            <img src={fem_black_rock_day} alt="Female black rocker daytime" style={{ width: '250px', height: '400px', margin: '10px', border: '2px solic #ccc' }} />
            <img src={male_col_bus_night} alt="Male black rocker daytime" style={{ width: '250px', height: '400px', margin: '10px', border: '2px solic #ccc' }} />
            <img src={fem_col_bus_day} alt="Female colorful business daytime" style={{ width: '250px', height: '400px', margin: '10px', border: '2px solic #ccc' }} />
            </div>
          </div>

        )
    }
}







export default HomeView