import React from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class PreferencesForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      option_selected: "",
      businessCount: 0,
      rockerCount: 0
    };
  }

  

  fetchPreferencesCount = () => {
    axios.get(`${API_URL}/users/count`, { withCredentials: true })
      .then(response => {
        console.log("API Response:", response.data); 
        const { businessCount, rockerCount } = response.data;
        this.setState({ businessCount, rockerCount });
      })
      .catch(err => {
        console.error("Error fetching preferences count:", err);
      });
  }
  componentDidMount() {
    this.fetchPreferencesCount();
  }

  handleOptionChange = (e) => {
    this.setState({ option_selected: e.target.value });
  }

  submitPreferences = (event) => {
    event.preventDefault();

    axios.post(`${API_URL}/users/preferences`, {
      option_selected: this.state.option_selected
    }, { withCredentials: true })
    .then(response => {
      if (response.data.success) {
        //alert("Preference saved successfully!");
        this.fetchPreferencesCount(); 
      } else {
        alert("Failed to save preferences.");
      }
    })
    .catch(err => {
      console.error("Error submitting preferences:", err);
    });
  }

  render() {
    const { businessCount, rockerCount } = this.state;
  
    return (
      <div>
        <h2>Select Your Preferences</h2>
        <form onSubmit={this.submitPreferences}>
          <label>
            <input 
              type="radio" 
              value="business" 
              checked={this.state.option_selected === 'business'} 
              onChange={this.handleOptionChange} 
            />
            Business
          </label>
          <label>
            <input 
              type="radio" 
              value="rocker" 
              checked={this.state.option_selected === 'rocker'} 
              onChange={this.handleOptionChange} 
            />
            Rocker
          </label>
        
          <button type="submit">Save Preferences</button>
        </form>
  
        <div>
          <h3>Currently popular:</h3>
          <p>Business: {businessCount} people</p>
          <p>Rocker: {rockerCount} people</p>
        </div>
      </div>
    );
  }
  
}

export default PreferencesForm;
