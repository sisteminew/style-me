import React, { Component } from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class FilesUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadStatus: '',
      images: [], // Array to store image data
      selectedFile: null,
      title: '',
      searchQuery: '',
      selectedFilename: '',
      newComment: '',
      comments: []
    };
  }

  componentDidMount() {
    this.fetchUploadedImages();
  }

  fetchUploadedImages = async () => {
    try {
      const response = await axios.get(`${API_URL}/uploads`);
      this.setState({
        images: response.data.map(image => ({
          url: `${API_URL}/imageuploads/${image.filename}`,
          title: image.title,
          filename: image.filename
        }))
      });
    } catch (error) {
      console.error('Error fetching images:', error);
    }
  }

  fetchComments = async (filename) => {
    try {
      const response = await axios.get(`${API_URL}/uploads/comments/${filename}`);
      this.setState({ comments: response.data, selectedFilename: filename });
    } catch (error) {
      console.error('Error fetching comments:', error);
    }
  }

  handleFileChange = (event) => {
    this.setState({ selectedFile: event.target.files[0] });
  }

  handleTitleChange = (event) => {
    this.setState({ title: event.target.value });
  }

  handleCommentChange = (event) => {
    this.setState({ newComment: event.target.value });
  }

  handleSubmit = async () => {
    const { selectedFile, title } = this.state;
    if (!selectedFile || !title) {
      this.setState({ uploadStatus: 'No file selected or title missing.' });
      return;
    }

    const formData = new FormData();
    formData.append('file', selectedFile);
    formData.append('title', title);

    try {
      await axios.post(`${API_URL}/uploads`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });

      // Refresh the image list
      this.fetchUploadedImages();
      this.setState({
        uploadStatus: 'File uploaded successfully.',
        title: '',
        selectedFile: null
      });
    } catch (error) {
      console.error('Error uploading file:', error);
      this.setState({ uploadStatus: 'Error uploading file.' });
    }
  }

  handleSearchChange = (event) => {
    this.setState({ searchQuery: event.target.value });
  }

  handleSearchSubmit = async (event) => {
    event.preventDefault();
    const { searchQuery } = this.state;

    try {
      const response = await axios.get(`${API_URL}/uploads/search?title=${searchQuery}`);
      const images = response.data;

      // Update the state with the complete image data
      this.setState({
        images: images.map(image => ({
          url: `${API_URL}/imageuploads/${image.filename}`,
          title: image.title,
          filename: image.filename
        }))
      });
    } catch (error) {
      console.error('Error searching images:', error);
      this.setState({ uploadStatus: 'Error searching images.' });
    }
  }

  handleCommentSubmit = async () => {
    const { newComment, selectedFilename } = this.state;
    if (!newComment || !selectedFilename) {
      alert('Comment cannot be empty');
      return;
    }

    try {
      await axios.post(`${API_URL}/uploads/comments`, {
        filename: selectedFilename,
        comment_text: newComment
      });

      // Refresh comments
      this.fetchComments(selectedFilename);
      this.setState({ newComment: '' });
    } catch (error) {
      console.error('Error adding comment:', error);
    }
  }

  render() {
    const { uploadStatus, images, title, searchQuery, comments, newComment, selectedFilename } = this.state;

    return (
      <div>
        <h1>Upload Image</h1>
        <input type="file" onChange={this.handleFileChange} />
        <input 
          type="text" 
          placeholder="Title" 
          value={title} 
          onChange={this.handleTitleChange} 
        />
        <button onClick={this.handleSubmit}>Upload</button>
        <p>{uploadStatus}</p>

        <h2>Search Images</h2>
        <form onSubmit={this.handleSearchSubmit}>
          <input 
            type="text" 
            placeholder="Search by title..." 
            value={searchQuery} 
            onChange={this.handleSearchChange} 
          />
          <button type="submit">Search</button>
        </form>

        <div id="imageGallery" className="image-gallery">
          {images.length > 0 ? (
            images.map((image, index) => (
              <div key={index} className="image-item">
                <img 
                  src={image.url} 
                  alt={image.title} 
                  onClick={() => this.fetchComments(image.filename)}
                />
                <p>{image.title}</p>
                {selectedFilename === image.filename && (
                  <div className="comments-section">
                    <textarea
                      value={newComment}
                      onChange={this.handleCommentChange}
                      placeholder="Add a comment"
                    />
                    <button onClick={this.handleCommentSubmit}>Submit Comment</button>
                    <div className="comments-list">
                      {comments.map((comment, i) => (
                        <div key={i}>
                          <p>{comment.comment_text}</p>
                          <small>{new Date(comment.created_at).toLocaleString()}</small>
                        </div>
                      ))}
                    </div>
                  </div>
                )}
              </div>
            ))
          ) : (
            <p>No images to display.</p>
          )}
        </div>
      </div>
    );
  }
}

export default FilesUpload;
