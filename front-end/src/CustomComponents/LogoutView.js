import React from "react";
import PropTypes from 'prop-types';
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

class LogoutView  { 

    static async logout() {
        try {
          const response = await axios.post(API_URL + '/users/logout', {}, { withCredentials: true })
          
          if (response.data && response.data.success && response.data.status.success) {
          return { success: true, msg: 'Logged out successfully' } 
        } else {
          return { success: false, msg: response.data.msg || 'Logout failed.' }
        }

      }catch (error) {
          console.error('Error during logout:', error)
          return { success: false, msg: 'Logout failed' }
        }
      }
}

export default LogoutView
