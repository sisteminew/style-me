import React from "react";
import axios from "axios";
import { API_URL } from "../Utils/Configuration";

import blue_outfit from '../outfits/blue_outfit.jpeg';
import pink_outfit from '../outfits/pink_outfit.jpeg';

class Colour extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pinkVotes: 0,
            blueVotes: 0
        };
    }

    componentDidMount() {
        this.fetchVotes();
    }

    fetchVotes = async () => {
        try {
            const response = await axios.get(API_URL + '/users/votes', { withCredentials: true });
            const data = response.data;
            this.setState({ pinkVotes: data.pink || 0, blueVotes: data.blue || 0 });
        } catch (error) {
            console.error('Failed to fetch votes:', error);
        }
    };

    handleVote = async (color) => {
        try {
            await axios.post(API_URL + '/users/vote', { color }, { withCredentials: true });
            this.fetchVotes(); // Refresh the votes after submitting
        } catch (error) {
            console.error('Failed to vote:', error);
        }
    };

    render() {
        const { pinkVotes, blueVotes } = this.state;
        const isTied = pinkVotes === blueVotes;
        const displayImage = !isTied ? (pinkVotes > blueVotes ? pink_outfit : blue_outfit) : null;

        return (
            <div className="card" style={{ margin: "10px" }}>
                <div className="image-container" style={{ margin: "10px" }}>
                    <img src={pink_outfit} alt="Pink outfit" style={{ width: '300px', height: '400px', margin: '10px', border: '2px solid #ccc' }} />
                    <img src={blue_outfit} alt="Blue outfit" style={{ width: '300px', height: '400px', margin: '10px', border: '2px solid #ccc' }} />
                </div>

                <div className="card-body">
                    <h5 className="card-title">Here you can vote on which outfit you think is better!</h5>
                    <h2>VOTE BELOW: (however many times you'd like!)</h2>
                    <button
                        onClick={() => this.handleVote('pink')}
                        style={{ padding: '10px 20px', fontSize: '16px', margin: '10px' }}
                    >
                        Pink
                    </button>
                    <button
                        onClick={() => this.handleVote('blue')}
                        style={{ padding: '10px 20px', fontSize: '16px', margin: '10px' }}
                    >
                        Blue
                    </button>
                    <div style={{ marginTop: '20px' }}>
                        <p>Pink Votes: {pinkVotes}</p>
                        <p>Blue Votes: {blueVotes}</p>
                    </div>
                    <div style={{ marginTop: '20px', textAlign: 'center' }}>
                        {isTied ? (
                            <h3>Tied! Vote above for your favorite!</h3>
                        ) : (
                            <>
                                <h3>Most Popular Outfit:</h3>
                                <img src={displayImage} alt="Most popular outfit" style={{ width: '300px', height: '400px', margin: '10px', border: '2px solid #ccc' }} />
                            </>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default Colour;
