
export const ABOUT = 'about';
export const NOVICE = 'novice';
export const ADDNEW = 'addnew';
export const SIGNUP = 'signup';
export const LOGIN = 'login';
export const NOVICA = 'novica';
export const HOME = 'home';
export const UPLOAD = 'upload';
export const LOGOUT = 'logout';
export const PREFERENCES = 'preferences';
export const COLOUR = 'colour';

