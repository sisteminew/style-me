const express = require('express');
const router = express.Router();
const DB = require('../db/dbConn'); 

// Route to handle preferences submission
router.post('/preferences', async (req, res) => {
  const userId = req.session.user[0].id; // Assuming user ID is stored in session
  const { option_selected } = req.body;

  if (!userId || !option_selected) {
    return res.status(400).json({ success: false, msg: "User ID or preferences not provided" });
  }

  try {
    // Insert preferences into the database
    await DB.SavePreferences(userId, option_selected);
    res.json({ success: true, msg: "Preferences saved successfully" });
  } catch (err) {
    console.error("Error saving preferences:", err);
    res.status(500).json({ success: false, msg: "Error saving preferences" });
  }
});

module.exports = router;
