const express = require("express")
const users = express.Router();
const DB = require('../db/dbConn.js')

const Cookies = require('cookies')


users.post('/login', async (req, res, next) => {
    try {
        console.log(req.body);
        const username = req.body.username;
        const password = req.body.password;
        if (username && password) {
            const queryResult = await DB.AuthUser(username, password)
            if (queryResult.length > 0) {
                if (password === queryResult[0].password) {
                    req.session.user = queryResult
                    req.session.logged_in = true
                    const cookies = new Cookies(req,res)
                    const cookieVal = encodeURIComponent(JSON.stringify(queryResult[0]))
                    cookies.set('user', cookieVal, { httpOnly: true, maxAge: 1000 * 60 * 24 * 10 })
                    res.status(200).json({ user: queryResult[0], status: { success: true, msg: "Logged in" } })
                } 
                else {
                    res.status(200).json({ user: null, status: { success: false, msg: "Password incorrect" } })
                    console.log("incorrect password")
                }
            } else {
                res.status(200).send({ user: null, status: { success: false, msg: "Username not registered" } })
            }
        }
        else {
            res.status(200).send({ logged: false, user: null, status: { success: false, msg: "Input element missing" } })
            console.log("Please enter Username and Password!")
        }
        res.end();
    } catch (err) {
        console.log(err)
        res.sendStatus(500)
        next()
    }
});

users.get('/session', async (req, res, next) => {
    try {
        if (req.session) {
            const user = await DB.getUserById(user_id)
            res.json({
                authenticated: true,
                user: user[0]
            })
        } 
        else {
            res.json({ authenticated: false})
        }


    } catch (error) {
        res.sendStatus(500)
    }
})

//Inserts a new user in our database id field are complete
users.post('/register', async (req, res, next) => {
    try {
        const username = req.body.username
        const password = req.body.password
        const email = req.body.email
        if (username && password && email) {
            const queryResult = await DB.AddUser(username, email, password);
            if (queryResult.affectedRows) {
                const cookies = new Cookies (req,res) 
                cookies.set('user', JSON.stringify({ username }), { httpOnly: true, maxAge: 1000 * 60 * 60 * 24 * 10})
                res.status(200).send({ status: { success: true, msg: "New user created" } })
                console.log("New user added!!")
            }
        }
        else {
            res.status(200).send({ status: { success: false, msg: "Input element missing" } })
            console.log("A field is missing!")
        }
        res.end();
    } catch (err) {
        console.log(err)
        res.status(500).send({ status: { success: false, msg: err } })
        next(err)
    }

});

users.get('/preferences', async (req, res, next) => {
        
        if (!req.session.userId) return res.redirect('/login');
        res.sendFile('/preferences.html') //not sure
})

users.post('/preferences', async (req, res, next) => {
    try {
        const user_id = req.session.user_id
        const option_selected = req.body

        const queryResult = await DB.AddPreferences(user_id, option_selected)

        const style_id = determineStyle(option_selected)

        const queryResult2 = await DB.UserStyle(user_id, style_id)
        res.status(200).json({ success: true, style: style });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
        next();
}})


function determineStyle(questions) {
    const business = []
    const rocker = []
    const casual = []

    let businessScore, rockerScore, casualScore = 0

    questions.forEach(question => {
        if (business.includes(question)) businessScore++
        if (rocker.includes(question)) rockerScore++
        if (casual.includes(question)) casualScore++
    })

    if (businessScore > 0) return 'business'
    if (rockerScore > 0) return 'rocker'
    if (casualScore > 0) return 'casual'
}



module.exports = users
