const mysql = require('mysql2');

const conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS, 
    database: process.env.DB_DATABASE
  })

  setInterval(() => {
    conn.query('SELECT 1', (error, result) => {
        if (error) {
            console.error('Error with keep-alive query:', error);
        } else {
            console.log('Keep-alive query executed successfully');
        }
    });
}, 60000);


 conn.connect((err) => {
      if(err){
          console.log("ERROR: " + err.message);
          return;    
      }
      console.log('Connection established');
    })


    let dataPool={}
  
dataPool.allNovice=()=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`SELECT * FROM news`, (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.oneNovica=(id)=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`SELECT * FROM news WHERE id = ?`, id, (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.creteNovica=(title,slug,text,file)=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`INSERT INTO news (title,slug,text, file) VALUES (?,?,?,?)`, [title, slug, text, file], (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.AuthUser=(username, password)=>
{
  return new Promise ((resolve, reject)=>{
    conn.query('SELECT * FROM user (username,password) VALUES (?,?)', [username, password ], (err,res, fields)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })  
	
}

dataPool.AddUser=(username,email,password)=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`INSERT INTO user (username,password,email) VALUES (?,?,?)`, [username,password,email ], (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.AddPreferences=(user_id,option_selected) => {
  return new Promise ((resolve, reject) =>{
    conn.query('INSERT INTO preferences (user_id, option_selected) VALUES (user_id, ?)', [user_id, option_selected], (err,res) =>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.UserStyle = (preferences_id) => { //ovdje u style_id treba stavit 1 do 3 ovisi koji je look u pitanju 
  return new Promise ((resolve, reject) => {
    conn.query('UPDATE style SET style_id = ?')
  })
}

module.exports = dataPool;

